#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <GL/gl.h>

typedef int32_t i32;
typedef uint32_t u32;
typedef int32_t b32;


void DoDisplay();

int main (int ArgCount, char **Args)
{
  int WinWidth = 1000;
  int WinHeight = 1000;
  u32 WindowFlags = SDL_WINDOW_OPENGL;
  SDL_Init(SDL_INIT_VIDEO);

  assert(!SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1));
  assert(!SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8));
  SDL_Window *Window = SDL_CreateWindow("OpenGL Test", 0, 0, WinWidth, WinHeight, WindowFlags);
  SDL_SetWindowResizable(Window, SDL_TRUE);
  assert(Window);

  SDL_GLContext Context = SDL_GL_CreateContext(Window);

  b32 Running = 1;
  b32 FullScreen = 0;
  while (Running)
  {
    SDL_GetWindowSize(Window, &WinWidth, &WinHeight);
    SDL_Event Event;
    while (SDL_PollEvent(&Event))
    {
      if (Event.type == SDL_KEYDOWN)
      {
        switch (Event.key.keysym.sym)
        {
          case SDLK_ESCAPE:
            Running = 0;
            break;
          case 'f':
            FullScreen = !FullScreen;
            if (FullScreen)
            {
              SDL_SetWindowFullscreen(Window, WindowFlags | SDL_WINDOW_FULLSCREEN_DESKTOP);
            }
            else
            {
              SDL_SetWindowFullscreen(Window, WindowFlags);
            }
            break;
          default:
            break;
        }
      }
      else if (Event.type == SDL_QUIT)
      {
        Running = 0;
      }
    }

    glViewport(0, 0, WinWidth, WinHeight);
    /*glClearColor(1.f, 0.f, 1.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);*/
    DoDisplay();

    SDL_GL_SwapWindow(Window);
  }
  return 0;
}

#include <GL/glext.h>
 
/*int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutSetOption(GLUT_MULTISAMPLE, 8);
    if (argc > 1)
       glutInitDisplayMode(GLUT_MULTISAMPLE);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutDisplayFunc(DoDisplay);
    glutMainLoop();
 
    return 0;
}*/
 
void DoDisplay()
 
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(2, 0b0101010101010101);

    //glEnable(GL_LINE_SMOOTH);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_MULTISAMPLE);
    glHint(GL_MULTISAMPLE_FILTER_HINT_NV, GL_NICEST);
        GLint iMultiSample = 0;
        GLint iNumSamples = 0;
        glGetIntegerv(GL_SAMPLE_BUFFERS, &iMultiSample);
        glGetIntegerv(GL_SAMPLES, &iNumSamples);
        printf("MSAA on, GL_SAMPLE_BUFFERS = %d, GL_SAMPLES = %d\n", iMultiSample, iNumSamples);

 
    glPushMatrix();
 
    glTranslatef(-0.5f, 0.0f, 0.0f);
 
    // Nonboundary
    glBegin(GL_TRIANGLES);
    glEdgeFlag(GL_FALSE);
    // Flags edges as either boundary or nonboundary.
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(0.2f, -0.2f);
    glEnd();
 
    glTranslatef(0.5f, 0.0f, 0.0f);
 
    // Nonboundary 
    glBegin(GL_TRIANGLES);
    glVertex2f(0.0f, 0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(0.2f, -0.2f);
    glEnd();
 
    glTranslatef(0.5f, 0.0f, 0.0f);

    // Nonboundary
    glBegin(GL_TRIANGLES);
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);    
    glEnd();

    glTranslatef(0.0f, -0.3f, 0.0f);

    glBegin(GL_LINES_ADJACENCY);
    glVertex2f(0.0f, 0.1f);
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, 0.2f);
    glVertex2f(-0.2f, 0.3f);
    glEnd();

    glTranslatef(0.0f, -0.4f, 0.0f);
    glBegin(GL_LINES);
    glVertex2f(0.1f, 0.2f);
    glVertex2f(0.0f, 0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(-0.2f, 0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(-0.3f, 0.3f);
    glVertex2f(-0.4f, 0.5f);
    glVertex2f(-0.5f, 0.6f);
    glEnd();
    glTranslatef(0.0f, 0.4f, 0.0f);

    glTranslatef(-1.0f, 0.3f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glTranslatef(0.0f, -0.9f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glTranslatef(0.9f, 0.0f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();

    glPopMatrix();
 
    glFlush();
}

