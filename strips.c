#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glut.h>
 
void DoDisplay();
 
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutDisplayFunc(DoDisplay);
    glutMainLoop();
 
    return 0;
}
 
void DoDisplay()
 
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(2, 0b0101010101010101);
 
    glPushMatrix();

    glScalef(0.1, 0.1, 0.1);
 
    glBegin(GL_QUAD_STRIP);
    glVertex3f(-5.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glVertex3f( -5.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glVertex3f( 0.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glVertex3f( 0.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glVertex3f( 5.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glVertex3f( 5.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glEnd();
 
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glTranslatef(0.0f, -4.9f, 0.0f);

    glBegin(GL_QUAD_STRIP);
    glVertex3f(-5.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glVertex3f( -5.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glVertex3f( 0.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glVertex3f( 0.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glVertex3f( 5.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glVertex3f( 5.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glEnd();

    glPopMatrix();
 
    glFlush();
}

