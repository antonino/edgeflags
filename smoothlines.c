#include <GL/gl.h>
#include <GL/glext.h>
//#include <GL/glut.h>
#include <GL/freeglut.h>
 
void DoDisplay();
 
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutSetOption(GLUT_MULTISAMPLE, 8);
    if (argc > 1)
       glutInitDisplayMode(GLUT_MULTISAMPLE);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutDisplayFunc(DoDisplay);
    glutMainLoop();
 
    return 0;
}
 
void DoDisplay()
 
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(2, 0b0101010101010101);

    //glEnable(GL_LINE_SMOOTH);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_MULTISAMPLE);
    glHint(GL_MULTISAMPLE_FILTER_HINT_NV, GL_NICEST);
        GLint iMultiSample = 0;
        GLint iNumSamples = 0;
        glGetIntegerv(GL_SAMPLE_BUFFERS, &iMultiSample);
        glGetIntegerv(GL_SAMPLES, &iNumSamples);
        printf("MSAA on, GL_SAMPLE_BUFFERS = %d, GL_SAMPLES = %d\n", iMultiSample, iNumSamples);

 
    glPushMatrix();
 
    glTranslatef(-0.5f, 0.0f, 0.0f);
 
    // Nonboundary
    glBegin(GL_TRIANGLES);
    glEdgeFlag(GL_FALSE);
    // Flags edges as either boundary or nonboundary.
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(0.2f, -0.2f);
    glEnd();
 
    glTranslatef(0.5f, 0.0f, 0.0f);
 
    // Nonboundary 
    glBegin(GL_TRIANGLES);
    glVertex2f(0.0f, 0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(0.2f, -0.2f);
    glEnd();
 
    glTranslatef(0.5f, 0.0f, 0.0f);

    // Nonboundary
    glBegin(GL_TRIANGLES);
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);    
    glEnd();

    glTranslatef(0.0f, -0.3f, 0.0f);

    glBegin(GL_LINES_ADJACENCY);
    glVertex2f(0.0f, 0.1f);
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, 0.2f);
    glVertex2f(-0.2f, 0.3f);
    glEnd();

    glTranslatef(0.0f, -0.4f, 0.0f);
    glBegin(GL_LINES);
    glVertex2f(0.1f, 0.2f);
    glVertex2f(0.0f, 0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(-0.2f, 0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(-0.3f, 0.3f);
    glVertex2f(-0.4f, 0.5f);
    glVertex2f(-0.5f, 0.6f);
    glEnd();
    glTranslatef(0.0f, 0.4f, 0.0f);

    glTranslatef(-1.0f, 0.3f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glTranslatef(0.0f, -0.9f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glTranslatef(0.9f, 0.0f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();

    glPopMatrix();
 
    glFlush();
}

