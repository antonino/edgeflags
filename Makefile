_BINS = colors llvmptest lvptest lvpworks main sdllines smoothlines smoothpoints stipple strips pointcoord subdata stripscolors pvtest border_color indirecttest
BINS = $(patsubst %,bin/%,$(_BINS))

all: mktd $(BINS)

bin/%: %.c
	gcc $< -g -lGL -lglut -lSDL2 -lGLU -o $@

mktd:
	mkdir -p bin/
