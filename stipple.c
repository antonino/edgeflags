#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glut.h>
 
void DoDisplay();
 
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutDisplayFunc(DoDisplay);
    glutMainLoop();
 
    return 0;
}
 
void DoDisplay()
 
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_LINE_STIPPLE);
    glEnable(GL_LINE_SMOOTH);
    glLineStipple(2, 0b0101010101010101);
 
    glPushMatrix();
 
    glTranslatef(-0.5f, 0.0f, 0.0f);
    glBegin(GL_LINES);
    glVertex2f(0.1f, 0.2f);
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, 0.2f);
    glVertex2f(-0.3f, 0.3f);
    glVertex2f(-0.4f, 0.5f);
    glVertex2f(-0.5f, 0.6f);
    glEnd();

    glPopMatrix();
 
    glFlush();
}

