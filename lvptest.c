#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <GL/gl.h>
#include <GL/glext.h>
#include <SDL2/SDL_opengl_glext.h>

typedef int32_t i32;
typedef uint32_t u32;
typedef int32_t b32;


void DoDisplay();

struct vert {
    float vertex[2];
    float smooth_color[4];
    float flat_color[4];
};

static struct vert quads_vertices[] = {
    {{   2,  2 }, {0.0, 1.0, 1.0, 1.0}, {1.0, 1.0, 1.0, 1.0}},
	  {{   2, 62 }, {1.0, 0.0, 1.0, 1.0}, {1.0, 1.0, 1.0, 1.0}},
	  {{  62, 62 }, {1.0, 1.0, 0.0, 1.0}, {1.0, 1.0, 1.0, 1.0}},
	  {{  62,  2 }, {0.0, 1.0, 1.0, 1.0}, {1.0, 1.0, 1.0, 1.0}},
	  {{ 102,  2 }, {1.0, 0.0, 1.0, 1.0}, {1.0, 1.0, 1.0, 1.0}},
	  {{ 102, 62 }, {1.0, 1.0, 0.0, 1.0}, {1.0, 1.0, 1.0, 1.0}},
	  {{ 162, 62 }, {0.0, 1.0, 1.0, 1.0}, {1.0, 1.0, 1.0, 1.0}},
	  {{ 162,  2 }, {1.0, 0.0, 1.0, 1.0}, {1.0, 1.0, 1.0, 1.0}}
};

static const char *vstext =
	"#version 130\n"
	"uniform vec2 vertex_offset;\n"
	"in vec2 vertex;\n"
	"in vec4 smooth_color;\n"
	"in vec4 flat_color;\n"
	"out vec2 vertex_varying;\n"
	"out vec4 smooth_color_varying;\n"
	"flat out vec4 flat_color_varying;\n"
	"\n"
	"void main()\n"
	"{\n"
	"  gl_Position = vec4(vertex + vertex_offset, 0, 128.0);\n"
	"  vertex_varying = vertex;\n"
	"  smooth_color_varying = smooth_color;\n"
	"  flat_color_varying = flat_color;\n"
	"}\n";

static const char *fstext =
	"#version 130\n"
	"uniform bool use_flat_color;\n"
	"in vec4 smooth_color_varying;\n"
	"flat in vec4 flat_color_varying;\n"
	"\n"
	"void main()\n"
	"{\n"
	"  vec4 color = use_flat_color ? flat_color_varying\n"
	"                              : smooth_color_varying;\n"
	"  if (!gl_FrontFacing)\n"
	"    color *= 0.5;\n"
	"  gl_FragColor = color;\n"
	"}\n";

GLuint vs, fs, prog;


GLuint compile_shader(char* src, GLenum type) {
    GLuint shaderi = glCreateShader(type);
    glShaderSource(shaderi, 1, &src, NULL);
    glCompileShader(shaderi);
    GLint isCompiled = 0;
    glGetShaderiv(shaderi, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(shaderi, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        GLchar *errorLog = (GLchar*)malloc(maxLength);
        glGetShaderInfoLog(shaderi, maxLength, &maxLength, &errorLog[0]);

        // Provide the infolog in whatever manor you deem best.
        // Exit with failure.
        glDeleteShader(shaderi); // Don't leak the shader.
        abort();
    }
    return shaderi;
}

void init() {
    vs = compile_shader(vstext, GL_VERTEX_SHADER);
    fs = compile_shader(fstext, GL_FRAGMENT_SHADER);

    prog = glCreateProgram();
    glAttachShader(prog, vs);
    glAttachShader(prog, fs);
    glLinkProgram(prog);

    glFrontFace(GL_CW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

 	GLint smooth_color_index = glGetAttribLocation(prog, "smooth_color");
  	GLint flat_color_index = glGetAttribLocation(prog, "flat_color");
    GLint vertex_index = glGetAttribLocation(prog, "vertex");

    glVertexAttribPointer(vertex_index, 2, GL_FLOAT, GL_FALSE,
              sizeof(quads_vertices[0]), &quads_vertices[0].vertex);
    glVertexAttribPointer(smooth_color_index, 4, GL_FLOAT, GL_FALSE,
                  sizeof(quads_vertices[0]), &quads_vertices[0].smooth_color);
    glVertexAttribPointer(flat_color_index, 4, GL_FLOAT, GL_FALSE,
                  sizeof(quads_vertices[0]), &quads_vertices[0].flat_color);


    glEnableVertexAttribArray(vertex_index);
    glEnableVertexAttribArray(smooth_color_index);
    glEnableVertexAttribArray(flat_color_index);
}

int main (int ArgCount, char **Args)
{
  int WinWidth = 1000;
  int WinHeight = 1000;
  u32 WindowFlags = SDL_WINDOW_OPENGL;
  SDL_Init(SDL_INIT_VIDEO);

  //assert(!SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1));
  //assert(!SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8));
  SDL_Window *Window = SDL_CreateWindow("OpenGL Test", 0, 0, WinWidth, WinHeight, WindowFlags);
  SDL_SetWindowResizable(Window, SDL_TRUE);
  assert(Window);

  SDL_GLContext Context = SDL_GL_CreateContext(Window);

  init();

  b32 Running = 1;
  b32 FullScreen = 0;
  while (Running)
  {
    SDL_GetWindowSize(Window, &WinWidth, &WinHeight);
    SDL_Event Event;
    while (SDL_PollEvent(&Event))
    {
      if (Event.type == SDL_KEYDOWN)
      {
        switch (Event.key.keysym.sym)
        {
          case SDLK_ESCAPE:
            Running = 0;
            break;
          case 'f':
            FullScreen = !FullScreen;
            if (FullScreen)
            {
              SDL_SetWindowFullscreen(Window, WindowFlags | SDL_WINDOW_FULLSCREEN_DESKTOP);
            }
            else
            {
              SDL_SetWindowFullscreen(Window, WindowFlags);
            }
            break;
          default:
            break;
        }
      }
      else if (Event.type == SDL_QUIT)
      {
        Running = 0;
      }
    }

    glViewport(0, 0, WinWidth, WinHeight);
    /*glClearColor(1.f, 0.f, 1.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);*/
    DoDisplay();

    SDL_GL_SwapWindow(Window);
  }
  return 0;
}

/*int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutSetOption(GLUT_MULTISAMPLE, 8);
    if (argc > 1)
       glutInitDisplayMode(GLUT_MULTISAMPLE);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutDisplayFunc(DoDisplay);
    glutMainLoop();
 
    return 0;
}*/
 
void DoDisplay()
{



    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
    glUseProgram(prog);

    float vertex_offset[2] = { -82.0, 0.0 };

    /*glUniform1i(glGetUniformLocation(prog, "use_flat_color"),
                1);
    glUniform2fv(glGetUniformLocation(prog, "vertex_offset"),
                 1, vertex_offset);*/

    glDrawArrays(GL_QUADS, 0, 8);
}
