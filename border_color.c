#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <GL/gl.h>
#include <GL/glext.h>
#include <SDL2/SDL_opengl_glext.h>

typedef int32_t i32;
typedef uint32_t u32;
typedef int32_t b32;


void DoDisplay();

static const char *vstext =
  "#version 130\n"
  "#extension GL_ARB_shading_language_420pack : require\n"
	"in vec2 vertex;\n"
	"out vec2 uv;\n"
	"void main()\n"
	"{\n"
	"  gl_Position = vec4(vertex, 0, 0.5);\n"
  "  vec2 uvs[] = {vec2(0.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 1.0), vec2(1.0, 0.0)};\n"
  "  uv = uvs[gl_VertexID];\n"
	"}\n";

static const char *fstext =
	"#version 430\n"
	"\n"
	"uniform sampler2D tex;\n"
	"uniform sampler2D tex1;\n"
  "out vec4 FragColor;"
	"in vec2 uv;\n"
  "layout(std430, binding = 3) buffer layoutName"
  "{                                            "
  "    vec4 data_SSBO;                         "
  "};                                           "
	"void main()\n"
	"{\n"
  "  vec2 tuv = uv * 2.0 - vec2(0.5);"
  "  vec2 offset = vec2(0.5)/vec2(200);\n"
  //"  gl_FragColor = textureLod(tex, uv * 2.0 - vec2(0.5), 5.5);\n"
  "  FragColor = tuv.y < 0.5 ? texture(tex, tuv + offset) : texture(tex1, tuv + offset) + data_SSBO;\n"
	"}\n";

GLuint vs, fs, prog;


GLuint compile_shader(char* src, GLenum type) {
    GLuint shaderi = glCreateShader(type);
    glShaderSource(shaderi, 1, &src, NULL);
    glCompileShader(shaderi);
    GLint isCompiled = 0;
    glGetShaderiv(shaderi, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(shaderi, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        GLchar *errorLog = (GLchar*)malloc(maxLength);
        glGetShaderInfoLog(shaderi, maxLength, &maxLength, &errorLog[0]);
        puts(errorLog);

        // Provide the infolog in whatever manor you deem best.
        // Exit with failure.
        glDeleteShader(shaderi); // Don't leak the shader.
        abort();
    }
    return shaderi;
}

float pointSizeRange[2];

#include "tux.c"

unsigned int texture, texture1;
void init() {
    vs = compile_shader(vstext, GL_VERTEX_SHADER);
    fs = compile_shader(fstext, GL_FRAGMENT_SHADER);

    prog = glCreateProgram();
    glAttachShader(prog, vs);
    glAttachShader(prog, fs);
    glLinkProgram(prog);

    glUseProgram(prog);
    glUniform1i(glGetUniformLocation(prog, "tex1"), 1);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, gimp_image.width, gimp_image.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, gimp_image.pixel_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, (GLfloat[]){1.0, 0.0, 0.0, 1.0});

    glGenerateMipmap(GL_TEXTURE_2D);

    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, gimp_image.width, gimp_image.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, gimp_image.pixel_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, (GLfloat[]){0.0, 1.0, 0.0, 1.0});

    glGenerateMipmap(GL_TEXTURE_2D);

    GLuint ssbo;
    glGenBuffers(1, &ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
    float v[] = {0.1, 0.2, 0.3, 0.0};
    for(int i = 0; i < 4; i++) v[i] = 0;
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(v), &v, GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, ssbo);
}

int main (int ArgCount, char **Args)
{
  int WinWidth = 1000;
  int WinHeight = 1000;
  u32 WindowFlags = SDL_WINDOW_OPENGL;
  SDL_Init(SDL_INIT_VIDEO);

  SDL_Window *Window = SDL_CreateWindow("OpenGL Test", 0, 0, WinWidth, WinHeight, WindowFlags);
  SDL_SetWindowResizable(Window, SDL_TRUE);
  assert(Window);

  SDL_GLContext Context = SDL_GL_CreateContext(Window);

  init();

  b32 Running = 1;
  b32 FullScreen = 0;
  while (Running)
  {
    SDL_GetWindowSize(Window, &WinWidth, &WinHeight);
    SDL_Event Event;
    while (SDL_PollEvent(&Event))
    {
      if (Event.type == SDL_KEYDOWN)
      {
        switch (Event.key.keysym.sym)
        {
          case SDLK_ESCAPE:
            Running = 0;
            break;
          case 'f':
            FullScreen = !FullScreen;
            if (FullScreen)
            {
              SDL_SetWindowFullscreen(Window, WindowFlags | SDL_WINDOW_FULLSCREEN_DESKTOP);
            }
            else
            {
              SDL_SetWindowFullscreen(Window, WindowFlags);
            }
            break;
          default:
            break;
        }
      }
      else if (Event.type == SDL_QUIT)
      {
        Running = 0;
      }
    }

    glViewport(0, 0, WinWidth, WinHeight);
    DoDisplay();

    SDL_GL_SwapWindow(Window);
  }
  return 0;
}

void DoDisplay()
{

    glUseProgram(prog);
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture1);

    glBegin(GL_QUADS);
    glVertex2f(-0.2, -0.2);
    glVertex2f(0.2, -0.2);
    glVertex2f(0.2, 0.2);
    glVertex2f(-0.2, 0.2);
    glEnd();

    glFlush();
}
