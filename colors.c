#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glut.h>
 
void DoDisplay();
 
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutDisplayFunc(DoDisplay);
    glutMainLoop();
 
    return 0;
}
 
void DoDisplay()
 
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(2, 0b0101010101010101);
 
    glPushMatrix();
    glProvokingVertex(GL_LAST_VERTEX_CONVENTION);

    glTranslatef(0.0f, 0.5f, 0.0f);
    glTranslatef(-0.7f, 0.0f, 0.0f);

    // Nonboundary
    glBegin(GL_TRIANGLES);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(0.0f, 0.2f);
    glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2f, -0.2f);
    glEnd();

    glTranslatef(0.41f, 0.0f, 0.0f);

    // Nonboundary
    glShadeModel(GL_FLAT);
    glBegin(GL_TRIANGLES_ADJACENCY);
    glEdgeFlag(GL_TRUE);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(0.0f, 0.2f);
    glVertex2f(0.0f, 0.0f);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2f, -0.2f);
    glVertex2f(0.0f, 0.0f);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2f, -0.2f);
    glVertex2f(0.0f, 0.0f);
    glEnd();
    glShadeModel(GL_SMOOTH);

    glTranslatef(0.45f, 0.0f, 0.0f);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(-0.2, -0.2);
    //glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(0.2, -0.2);
    //glEdgeFlag(GL_TRUE);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2, 0.2);
    //glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2, 0.2);

    glEnd();

    glTranslatef(0.45f, 0.0f, 0.0f);

    glShadeModel(GL_FLAT);
    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(-0.2, -0.2);
    //glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(0.2, -0.2);
    //glEdgeFlag(GL_TRUE);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2, 0.2);
    //glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2, 0.2);
    glEnd();
    glShadeModel(GL_SMOOTH);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glPopMatrix();
    glPushMatrix();
    glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
    glTranslatef(0.0f, -0.5f, 0.0f);
    glTranslatef(-0.7f, 0.0f, 0.0f);
    // Nonboundary
    glBegin(GL_TRIANGLES);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(0.0f, 0.2f);
    glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2f, -0.2f);
    glEnd();

    glTranslatef(0.41f, 0.0f, 0.0f);

    // Nonboundary
    glShadeModel(GL_FLAT);
    glBegin(GL_TRIANGLES);
    glEdgeFlag(GL_TRUE);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(0.0f, 0.2f);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2f, -0.2f);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2f, -0.2f);
    glEnd();
    glShadeModel(GL_SMOOTH);

    glTranslatef(0.45f, 0.0f, 0.0f);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(-0.2, -0.2);
    //glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(0.2, -0.2);
    //glEdgeFlag(GL_TRUE);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2, 0.2);
    //glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2, 0.2);

    glEnd();

    glTranslatef(0.45f, 0.0f, 0.0f);

    glShadeModel(GL_FLAT);
    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(-0.2, -0.2);
    //glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(0.2, -0.2);
    //glEdgeFlag(GL_TRUE);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2, 0.2);
    //glEdgeFlag(GL_FALSE);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2, 0.2);
    glEnd();
    glShadeModel(GL_SMOOTH);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glPopMatrix();
 
    glFlush();
}

