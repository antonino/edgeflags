//gcc llvmptest.c -g -lGL -lSDL2 -o llvmptest
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <GL/gl.h>
#include <GL/glext.h>
#include <SDL2/SDL_opengl_glext.h>

typedef int32_t i32;
typedef uint32_t u32;
typedef int32_t b32;


void DoDisplay();

static float quads_vertices[][2] = {
    {   2,  2 },
	  {   2, 62 },
	  {  62, 62 },
	  {  62,  2 },
	  { 102,  2 },
	  { 102, 62 },
	  { 162, 62 },
	  { 162,  2 },
};

static const char *vstext =
	"#version 130\n"
	"in vec2 vertex;\n"
	"\n"
	"void main()\n"
	"{\n"
	"  gl_Position = vec4(vertex, 0, 128.0);\n"
	"}\n";

static const char *fstext =
	"#version 130\n"
	"\n"
	"void main()\n"
	"{\n"
  "  vec4 color = vec4(1.0);\n"
	"  if (!gl_FrontFacing)\n"
	"    color *= 0.5;\n"
	"  gl_FragColor = color;\n"
	"}\n";

GLuint vs, fs, gs, prog;


GLuint compile_shader(char* src, GLenum type) {
    GLuint shaderi = glCreateShader(type);
    glShaderSource(shaderi, 1, &src, NULL);
    glCompileShader(shaderi);
    GLint isCompiled = 0;
    glGetShaderiv(shaderi, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(shaderi, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        GLchar *errorLog = (GLchar*)malloc(maxLength);
        glGetShaderInfoLog(shaderi, maxLength, &maxLength, &errorLog[0]);
        printf("errors \n%s\n", errorLog);

        // Provide the infolog in whatever manor you deem best.
        // Exit with failure.
        glDeleteShader(shaderi); // Don't leak the shader.
        abort();
    }
    return shaderi;
}

typedef struct {
  GLuint count;
  GLuint primCount;
  GLuint first;
  GLuint reservedMustBeZero;
} DrawArraysIndirectCommand;

void init() {
    vs = compile_shader(vstext, GL_VERTEX_SHADER);
    fs = compile_shader(fstext, GL_FRAGMENT_SHADER);

    prog = glCreateProgram();
    glAttachShader(prog, vs);
    glAttachShader(prog, fs);
    glLinkProgram(prog);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    GLint vertex_index = glGetAttribLocation(prog, "vertex");

    glVertexAttribPointer(vertex_index, 2, GL_FLOAT, GL_FALSE,
              sizeof(quads_vertices[0]), quads_vertices);


    glEnableVertexAttribArray(vertex_index);

    GLuint indirectBuf;
    //glGenBuffers(1, &indirectBuf);
    glGenBuffers(1, &indirectBuf);
    glBindBuffer(GL_DRAW_INDIRECT_BUFFER, indirectBuf);
    DrawArraysIndirectCommand c[] = {{3, 1, 0, 0}, {3, 1, 3, 0}};
    glBufferData(GL_DRAW_INDIRECT_BUFFER, sizeof(DrawArraysIndirectCommand)*2, &c[0],  GL_STATIC_DRAW);
}

int main (int ArgCount, char **Args)
{
  int WinWidth = 1000;
  int WinHeight = 1000;
  u32 WindowFlags = SDL_WINDOW_OPENGL;
  SDL_Init(SDL_INIT_VIDEO);

  //assert(!SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1));
  //assert(!SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8));
  SDL_Window *Window = SDL_CreateWindow("OpenGL Test", 0, 0, WinWidth, WinHeight, WindowFlags);
  SDL_SetWindowResizable(Window, SDL_TRUE);
  assert(Window);

  SDL_GLContext Context = SDL_GL_CreateContext(Window);

  init();

  b32 Running = 1;
  b32 FullScreen = 0;
  while (Running)
  {
    SDL_GetWindowSize(Window, &WinWidth, &WinHeight);
    SDL_Event Event;
    while (SDL_PollEvent(&Event))
    {
      if (Event.type == SDL_KEYDOWN)
      {
        switch (Event.key.keysym.sym)
        {
          case SDLK_ESCAPE:
            Running = 0;
            break;
          case 'f':
            FullScreen = !FullScreen;
            if (FullScreen)
            {
              SDL_SetWindowFullscreen(Window, WindowFlags | SDL_WINDOW_FULLSCREEN_DESKTOP);
            }
            else
            {
              SDL_SetWindowFullscreen(Window, WindowFlags);
            }
            break;
          default:
            break;
        }
      }
      else if (Event.type == SDL_QUIT)
      {
        Running = 0;
      }
    }

    glViewport(0, 0, WinWidth, WinHeight);
    DoDisplay();

    SDL_GL_SwapWindow(Window);
  }
  return 0;
}


void DoDisplay()
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
    glUseProgram(prog);

    //glDrawArrays(GL_TRIANGLES, 0, 8);
    glMultiDrawArraysIndirect(GL_TRIANGLES, 0, 2, 0);
}
