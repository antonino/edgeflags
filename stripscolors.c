#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glut.h>
 
void DoDisplay();
 
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutDisplayFunc(DoDisplay);
    glutMainLoop();
 
    return 0;
}

void draw_shapes() {
    // Nonboundary
    glBegin(GL_LINE_STRIP);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(-0.2f, 0.0f);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(0.0f, 0.0f);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2f, 0.0f);
    glEnd();

    glTranslatef(0.0f, -0.41f, 0.0f);

    glBegin(GL_TRIANGLE_STRIP);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(0.0f, 0.2f);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2f, -0.2f);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2f, -0.2f);
    glEnd();

    glTranslatef(0.0f, -0.41f, 0.0f);
    glScalef(0.2, 0.2, 1.0);

    glBegin(GL_QUAD_STRIP);
    glColor3f(0.0, 1.0, 1.0);
    glVertex3f(-5.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glColor3f(1.0, 1.0, 1.0);
    glVertex3f( -5.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f( 0.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glColor3f(1.0, 0.0, 1.0);
    glVertex3f( 0.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f( 5.0f/10.0, -5.0f/10.0, 0.0f/10.0);
    glColor3f(1.0, 1.0, 0.0);
    glVertex3f( 5.0f/10.0, 5.0f/10.0, 0.0f/10.0);
    glColor3f(0.0, 0.0, 0.0);
    glEnd();

}

void DoDisplay()
 
{
    glClear(GL_COLOR_BUFFER_BIT);

    glPushMatrix();
    glProvokingVertex(GL_LAST_VERTEX_CONVENTION);
    glShadeModel(GL_FLAT);

    glTranslatef(0.0f, 0.5f, 0.0f);
    glTranslatef(-0.7f, 0.0f, 0.0f);

    draw_shapes();

    glPopMatrix();
    glPushMatrix();
    glTranslatef(0.7f, 0.0f, 0.0f);
    glTranslatef(0.0f, 0.5f, 0.0f);
    glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);

    draw_shapes();

    glPopMatrix();
    glFlush();
}
