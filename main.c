#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glut.h>
#include <stdio.h>
 
void DoDisplay();
 
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glutDisplayFunc(DoDisplay);
    glutMainLoop();
 
    return 0;
}
 
void DoDisplay()
 
{
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(2, 0b0101010101010101);
 
    glPushMatrix();
 
    glTranslatef(-0.5f, 0.0f, 0.0f);

    GLuint qid;
    glGenQueries(1, &qid);
    glBeginQuery(0x88BF/* GL_ANY_SAMPLES_PASSED */, qid);
    GLenum err = glGetError();
    printf("err %x\n", err);

    // Nonboundary
    glBegin(GL_TRIANGLES);
    glEdgeFlag(GL_FALSE);
    // Flags edges as either boundary or nonboundary.
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(0.2f, -0.2f);
    glEnd();
 
    glTranslatef(0.5f, 0.0f, 0.0f);
 
    // Nonboundary 
    glBegin(GL_TRIANGLES);
    glVertex2f(0.0f, 0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(0.2f, -0.2f);
    glEnd();
 
    glTranslatef(0.5f, 0.0f, 0.0f);

    // Nonboundary
    glBegin(GL_TRIANGLES);
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, -0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(0.2f, -0.2f);
    glEdgeFlag(GL_TRUE);    
    glEnd();

    glTranslatef(0.0f, -0.3f, 0.0f);

    glBegin(GL_LINES_ADJACENCY);
    glVertex2f(0.0f, 0.1f);
    glVertex2f(0.0f, 0.2f);
    glVertex2f(-0.2f, 0.2f);
    glVertex2f(-0.2f, 0.3f);
    glEnd();

    glTranslatef(0.0f, -0.4f, 0.0f);
    glBegin(GL_LINES);
    glVertex2f(0.1f, 0.2f);
    glVertex2f(0.0f, 0.2f);
    glEdgeFlag(GL_FALSE);
    glVertex2f(-0.2f, 0.2f);
    glEdgeFlag(GL_TRUE);
    glVertex2f(-0.3f, 0.3f);
    glVertex2f(-0.4f, 0.5f);
    glVertex2f(-0.5f, 0.6f);
    glEnd();
    glTranslatef(0.0f, 0.4f, 0.0f);

    glTranslatef(-1.0f, 0.3f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glTranslatef(0.0f, -0.9f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glTranslatef(0.9f, 0.0f, 0.0f);

    glBegin(GL_QUADS);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(1.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(5.5/10.0, 1.5/10.0);
    //glEdgeFlag(GL_TRUE);
    glVertex2f(5.5/10.0, 5.5/10.0);
    //glEdgeFlag(GL_FALSE);
    glVertex2f(1.5/10.0, 5.5/10.0);

    glEnd();

    glPopMatrix();

   glEndQuery(0x88BF/* GL_ANY_SAMPLES_PASSED */);
   err = glGetError();
   printf("err %x\n", err);
   uint32_t samples = 0;
   glGetQueryObjectuiv(qid, GL_QUERY_RESULT, &samples);
   glDeleteQueries(1, &qid);
   printf("query res %li\n", samples);
 
    glFlush();
}

