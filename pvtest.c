//gcc llvmptest.c -g -lGL -lSDL2 -o llvmptest
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <GL/gl.h>
#include <GL/glext.h>
#include <SDL2/SDL_opengl_glext.h>

typedef int32_t i32;
typedef uint32_t u32;
typedef int32_t b32;


void DoDisplay();
/*
**
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(0.0f, 0.2f);
    glColor3f(0.0, 1.0, 0.0);
    glVertex2f(-0.2f, -0.2f);
    glColor3f(0.0, 0.0, 1.0);
    glVertex2f(0.2f, -0.2f);
*/
static float strip_vertices[][5] = {
    {   0.0f,  0.0f, 1.0f,  0.0f, 0.0f },
	  {   0.5f, 1.0f,  0.0f,  1.0f, 0.0f },
	  {   1.0f, 0.0f,  0.0f,  0.0f, 1.0f },
	  {   1.5f, 1.0f,  1.0f,  1.0f, 0.0f },
	  {   2.0f, 0.0f,  1.0f,  1.0f, 1.0f },
};

static const char *vstext =
	"#version 130\n"
	"in vec2 vertex;\n"
	"in vec3 color;\n"
	"\n"
	"void main()\n"
	"{\n"
	"  gl_Position = vec4(vertex - vec2(3.0), 0, 5.0);\n"
	"}\n";

static const char *fstext =
	"#version 130\n"
	"flat in vec3 fout_color;\n"
	"\n"
	"void main()\n"
	"{\n"
	"  gl_FragColor = vec4(fout_color, 1.0);\n"
	"}\n";

static const char *gstext =
  "#version 450\n"
  "\n"
  "layout (triangles) in;\n"
  "layout (triangle_strip, max_vertices = 9) out;\n"
  "in vec3 out_color[];\n"
  "out vec3 fout_color;\n"
  "void main() {\n"
  "    vec3 colors[] = {vec3(1.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 1.0)};\n"
  "    for (int i =0; i < 3; i++) {\n"
  "        for (int j =0; j < 3; j++) {\n"
  "            vec2 off[] = {vec2(0.0, 0.0), vec2(0.1, 0.2), vec2(0.2, 0.0)};\n"
  "            gl_Position = gl_in[i].gl_Position + vec4(gl_PrimitiveIDIn*1.3, 0.0, 0.0, 0.0)+vec4(off[j], 0.0, 0.0);\n"
  "            fout_color = colors[i];\n"
  "            EmitVertex();\n"
  "        }\n"
  "        EndPrimitive();\n"
  "    }\n"
  "\n"
  "}\n";

GLuint vs, fs, gs, prog;


GLuint compile_shader(char* src, GLenum type) {
    GLuint shaderi = glCreateShader(type);
    glShaderSource(shaderi, 1, &src, NULL);
    glCompileShader(shaderi);
    GLint isCompiled = 0;
    glGetShaderiv(shaderi, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(shaderi, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        GLchar *errorLog = (GLchar*)malloc(maxLength);
        glGetShaderInfoLog(shaderi, maxLength, &maxLength, &errorLog[0]);
        printf("errors \n%s\n", errorLog);

        // Provide the infolog in whatever manor you deem best.
        // Exit with failure.
        glDeleteShader(shaderi); // Don't leak the shader.
        abort();
    }
    return shaderi;
}

void init() {
    vs = compile_shader(vstext, GL_VERTEX_SHADER);
    fs = compile_shader(fstext, GL_FRAGMENT_SHADER);
    gs = compile_shader(gstext, GL_GEOMETRY_SHADER);

    prog = glCreateProgram();
    glAttachShader(prog, vs);
    glAttachShader(prog, fs);
    glAttachShader(prog, gs);
    glLinkProgram(prog);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    GLint vertex_index = glGetAttribLocation(prog, "vertex");
    GLint color_index = glGetAttribLocation(prog, "color");

    glVertexAttribPointer(vertex_index, 2, GL_FLOAT, GL_FALSE,
              sizeof(strip_vertices[0]), strip_vertices);
    glEnableVertexAttribArray(vertex_index);
    glVertexAttribPointer(color_index, 3, GL_FLOAT, GL_FALSE,
              sizeof(strip_vertices[0]), &strip_vertices[0][2]);
    glEnableVertexAttribArray(color_index);
}

int WinWidth = 1000;
int WinHeight = 1000;
int main (int ArgCount, char **Args)
{
  u32 WindowFlags = SDL_WINDOW_OPENGL;
  SDL_Init(SDL_INIT_VIDEO);

  //assert(!SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1));
  //assert(!SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8));
  SDL_Window *Window = SDL_CreateWindow("OpenGL Test", 0, 0, WinWidth, WinHeight, WindowFlags);
  SDL_SetWindowResizable(Window, SDL_TRUE);
  assert(Window);

  SDL_GLContext Context = SDL_GL_CreateContext(Window);

  init();

  b32 Running = 1;
  b32 FullScreen = 0;
  while (Running)
  {
    SDL_GetWindowSize(Window, &WinWidth, &WinHeight);
    SDL_Event Event;
    while (SDL_PollEvent(&Event))
    {
      if (Event.type == SDL_KEYDOWN)
      {
        switch (Event.key.keysym.sym)
        {
          case SDLK_ESCAPE:
            Running = 0;
            break;
          case 'f':
            FullScreen = !FullScreen;
            if (FullScreen)
            {
              SDL_SetWindowFullscreen(Window, WindowFlags | SDL_WINDOW_FULLSCREEN_DESKTOP);
            }
            else
            {
              SDL_SetWindowFullscreen(Window, WindowFlags);
            }
            break;
          default:
            break;
        }
      }
      else if (Event.type == SDL_QUIT)
      {
        Running = 0;
      }
    }

    glViewport(0, 0, WinWidth, WinHeight);
    DoDisplay();

    SDL_GL_SwapWindow(Window);
  }
  return 0;
}

void DoDisplay()
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glProvokingVertex(GL_LAST_VERTEX_CONVENTION);
    glUseProgram(prog);

    glViewport(0, 0, WinWidth, WinHeight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 5);
    glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
    glViewport(0, 200, WinWidth, WinHeight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 5);
}
