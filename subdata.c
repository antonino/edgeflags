#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <stdbool.h>
 
void DoDisplay();

const int piglit_width = 300, piglit_height = 300;

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutCreateWindow("OpenGL");
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glutDisplayFunc(DoDisplay);

    gluOrtho2D(0, piglit_width, 0, piglit_height);
    glutMainLoop();
 
    return 0;
}
 
void DoDisplay()
 
{
    uint32_t buffer_size = 4096;
    bool pass = true;
    GLuint vbo;
    float green[] = {0, 1, 0, 0};
    uint32_t count = piglit_width * piglit_height;
    int i;

    glClearColor(1, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glColor4fv(green);

    glGenBuffersARB(1, &vbo);
    glBindBufferARB(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, buffer_size, NULL, GL_STREAM_DRAW);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, NULL);

    for (i = 0; i < count; i++) {
        int x = i % piglit_width;
        int y = (i / piglit_height) % piglit_height;
        float vert[] = {
            x,     y,
            x + 1, y,
            x + 1, y + 1,
            x,     y + 1,
        };
        uint32_t offset = i % (buffer_size / sizeof(vert));
        uint32_t indices[4] = {
            offset * 4,
            offset * 4 + 1,
            offset * 4 + 2,
            offset * 4 + 3
        };

        glBufferSubData(GL_ARRAY_BUFFER, offset * sizeof(vert),
                sizeof(vert), vert);
        //glBufferData(GL_ARRAY_BUFFER, sizeof(vert), vert, GL_STREAM_DRAW);

            //glDrawArrays(GL_TRIANGLE_FAN, offset * 4, 4);
            glDrawElements(GL_TRIANGLE_FAN, 4, GL_UNSIGNED_INT,
                       indices);
            /*glDrawRangeElements(GL_TRIANGLE_FAN,
                        indices[0], indices[3],
                        4, GL_UNSIGNED_INT,
                        indices);*/

    }

    glFlush();
}
